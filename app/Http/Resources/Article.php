<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'article',
            'id' => $this->id,
            'attributes' => [
                'title' => $this->title,
            ],
            'links' => route('v1.articles.index', ['article' => $this->id])
        ];
    }
}
