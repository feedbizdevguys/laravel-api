<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    return [
        'article_id' => $faker->numberBetween($min = 1, $max = 5),
        'author_id' => $faker->numberBetween($min = 1, $max = 3),
        'body' => $faker->text,
    ];
});
