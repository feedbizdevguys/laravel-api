<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Article::class, function (Faker $faker) {
    return [
        'author_id' => $faker->numberBetween($min = 1, $max = 3),
        'title' => $faker->name,
    ];
});
