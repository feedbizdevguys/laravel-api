<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('articles')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        factory(App\Models\Article::class, 5)->create();
    }
}
